/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dialog.h"
#include <QFileDialog>
#include <QBuffer>
#include <QtCore/QDebug>

#define LOAD_FROM_FILE 0
#define LOAD_FROM_MEMORY 1
#define PLAY_MODE LOAD_FROM_FILE

Dialog::Dialog(QWidget *parent)
#if PLAY_MODE == LOAD_FROM_FILE
  : QDialog(parent), sharedMemory("sharedmemory_data")
#elif PLAY_MODE == LOAD_FROM_MEMORY
  : QDialog(parent), sharedMemory("sharedmemory_ret")
#endif
{
    ui.setupUi(this);
    connect(ui.loadFromFileButton, SIGNAL(clicked()), SLOT(loadFromFile()));
    connect(ui.loadFromSharedMemoryButton,
            SIGNAL(clicked()),
            SLOT(loadFromMemory()));
    setWindowTitle(tr("SharedMemory Example"));

    //sharedMemory.create(32+8+2764800);
}

Dialog::~Dialog()
{
    sharedMemory.detach();
}

#define SC(type, expr) static_cast<type>(expr)
#define RC(type, expr) reinterpret_cast<type>(expr)
#include <QImage>
void Dialog::loadFromFile()
{
#if 1
qDebug("WW 1");
    if (sharedMemory.isAttached())
        detach();

qDebug("WW 000");
#if PLAY_MODE == LOAD_FROM_FILE
    if (!sharedMemory.create(8+2764800)) {                  // for shr_dataslave_thread.py test
    //if (!sharedMemory.create(4 + 4 + 4 * 4 * 30)) {         // for shr_retmaster_thread.py test

        ui.label->setText(tr("Unable to create shared memory segment."));
        return;
    }
#elif PLAY_MODE == LOAD_FROM_MEMORY
    if (!sharedMemory.attach()) {
        ui.label->setText(tr("Unable to attach to shared memory segment.\n" \
                             "Load an image first."));
        return;
    }
#endif
qDebug("WW size=%d", sharedMemory.size());
    sharedMemory.lock();

    uint8_t *pData = RC(uint8_t*, sharedMemory.data());
qDebug("WW pData=%p", pData);
    uint8_t x[8]={0x12, 0x34, 0x56,0x78, 0xAA, 0xBB, 0xCC, 0xDD};
    int frameIdx = 1234;//0x00001234;
    int indicator = -1; // 0xFFFFFFFF
    //memcpy(pData, dstByteArray.data(), SC(size_t, dstByteArray.size()));
    //memcpy(pData, x, 4);
    int offset = 1280 * 720 * 3;

qDebug("WW 3");
    img = QImage(
QString("D:\\Work\\projects\\QtProjects\\build-sharedmemory2-Qt_5_9_5_in_PATH_qt595_build-Release\\release\\img_50.bmp"),
"bmp");

    qDebug("WW 4");
    img = img.convertToFormat(QImage::Format_RGB888);

qDebug("WW2 img.format()=%d img.depth()=%d", img.format(), img.depth()); //13, 24
    //qDebug("img.bytesPerLine() * img.height()=%d", 2764800);//img.bytesPerLine() * img.height());

    memcpy(pData, img.constBits(), 2764800);

    pData += offset;
    memcpy(pData, SC(const void*, &frameIdx), 4);
    memcpy(pData+4, SC(const void*, &indicator), 4);

    sharedMemory.unlock();
    qDebug("go");
#else
    if (sharedMemory.isAttached())
        detach();

    ui.label->setText(tr("Select an image file"));
    QString fileName = QFileDialog::getOpenFileName(0, QString(), QString(),
                                        tr("Images (*.png *.xpm *.jpg *.bmp)"));
    QImage image;
    if (!image.load(fileName)) {
        ui.label->setText(tr("Selected file is not an image, please select another."));
        return;
    }
    ui.label->setPixmap(QPixmap::fromImage(image));
//! [1] //! [2]

    // load into shared memory
    QBuffer buffer;
    buffer.open(QBuffer::ReadWrite);
    QDataStream out(&buffer);
    out << image;
    int size = buffer.size();

    if (!sharedMemory.create(size)) {
        ui.label->setText(tr("Unable to create shared memory segment."));
        return;
    }
    sharedMemory.lock();
    char *to = (char*)sharedMemory.data();
    const char *from = buffer.data().data();
    memcpy(to, from, qMin(sharedMemory.size(), size));
    sharedMemory.unlock();
#endif
}

__inline
uint32_t GetValue(const uint8_t *pData, uint32_t offset)
{
    return (((pData[3+offset] << 24) & 0xFF000000) |
            ((pData[2+offset] << 16) & 0x00FF0000) |
            ((pData[1+offset] <<  8) & 0x0000FF00) |
            ((pData[0+offset] ) & 0x000000FF));
}

void Dialog::loadFromMemory()
{
#if PLAY_MODE == LOAD_FROM_FILE
    if (!sharedMemory.attach()) {
        ui.label->setText(tr("Unable to attach to shared memory segment.\n" \
                             "Load an image first."));
        return;
    }
#elif PLAY_MODE == LOAD_FROM_MEMORY
    if (!sharedMemory.create(4 + 4 + 4 * 4 * 30)) {         // for shr_retmaster_thread.py test

        ui.label->setText(tr("Unable to create shared memory segment."));
        return;
    }
#endif


#if 1

    sharedMemory.lock();
    const uint8_t *pData = (const uint8_t*)sharedMemory.constData();
    qDebug("sharedMemory.size()=%d", sharedMemory.size());

    uint32_t idx = 0;
    uint32_t len = GetValue(pData, 0);
    idx += 4;
    qDebug("len=%d", len);
    for (uint32_t i=0;i<len;++i)
    {
        qDebug("GetValue(pData, %d)=%d", idx, GetValue(pData, idx));
        idx += 4;
        qDebug("GetValue(pData, %d)=%d", idx, GetValue(pData, idx));
        idx += 4;
        qDebug("GetValue(pData, %d)=%d", idx, GetValue(pData, idx));
        idx += 4;
        qDebug("GetValue(pData, %d)=%d", idx, GetValue(pData, idx));
        idx += 4;
    }
    sharedMemory.unlock();

#else

    QBuffer buffer;
    QDataStream in(&buffer);
    QImage image;

    sharedMemory.lock();
    buffer.setData((char*)sharedMemory.constData(), sharedMemory.size());
    buffer.open(QBuffer::ReadOnly);
    in >> image;
    sharedMemory.unlock();

    sharedMemory.detach();
    ui.label->setPixmap(QPixmap::fromImage(image));
#endif

}
//! [3]

/*!
  This private function is called by the destructor to detach the
  process from its shared memory segment. When the last process
  detaches from a shared memory segment, the system releases the
  shared memory.
 */
void Dialog::detach()
{
    if (!sharedMemory.detach())
        ui.label->setText(tr("Unable to detach from shared memory."));
}

