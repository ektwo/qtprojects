#ifndef VIDEOHOLDER_H
#define VIDEOHOLDER_H

#include <QObject>
#include <QMutex>
#include <QSemaphore>
#include <QTimerEvent>
#include "videoprovider.h"
#include "videorenderer.h"

#ifdef QT_VERSION
#include <QDebug>
#define DSG(mode, __format__, ...) { if (1 == mode) qDebug(__format__, ##__VA_ARGS__); }
#else
#ifdef __linux__
#define DSG(mode, __format__, ...) { if (1 == mode) printf(__format__, ##__VA_ARGS__); }
#elif _WIN32
#include <Windows.h>
#define MAX_DBG_MSG_LEN (1024)
static void DSG(int mode, const char * format, ...)
{
    if (1 == mode) {
        char buf[MAX_DBG_MSG_LEN];
        va_list ap;
        va_start(ap, format);
        _vsnprintf(buf, sizeof(buf), format, ap);
        va_end(ap);
        OutputDebugStringA(buf);
    }
}
#endif
#endif

class VideoHolder : public QObject
{
    Q_OBJECT
public:
    explicit VideoHolder(QObject *parent = nullptr);
    ~VideoHolder();

    void Stop();

public slots:
    void handleTest(int loopCount, int capacity);

protected:
    void timerEvent(QTimerEvent * event);



public:
    int loopCount;
    int capacity;
    QMutex mutex;
    QSemaphore *m_semProduct;
    QSemaphore *m_semRequestSpace;

private:
    VideoProvider *m_pVideoProvider;
    VideoRenderer *m_pVideoRenderer;
};

#endif // VIDEOQUEUE_H
