#include "videoholder.h"

template <class T> void SafeRelease(T **ppT)
{
    if (*ppT)
    {
        (*ppT)->Release();
        *ppT = NULL;
    }
}
template<typename T> void SafeDelete(T*& a) {
  delete a;
  a = NULL;
}

VideoHolder::VideoHolder(QObject *parent)
    : QObject(parent)
    , m_pVideoProvider(nullptr)
    , m_pVideoRenderer(nullptr)
{
    DSG(1, "VideoHolder QThread::currentThreadId()=%d", QThread::currentThreadId());
}
VideoHolder::~VideoHolder()
{
    SafeDelete(m_pVideoProvider);
    SafeDelete(m_pVideoRenderer);
}

void VideoHolder::Stop()
{
    DSG(1, "VideoHolder::Stop()");
    if (m_pVideoProvider)
        m_pVideoProvider->requestInterruption();
    if (m_pVideoRenderer)
        m_pVideoRenderer->requestInterruption();
    DSG(1, "VideoHolder::Stop() 1");
}

void VideoHolder::handleTest(int loopCount, int capacity)
{
    this->loopCount = loopCount;
    this->capacity = capacity;

    this->m_semProduct = new QSemaphore(0);
    this->m_semRequestSpace = new QSemaphore(capacity);

    m_pVideoProvider = new VideoProvider(this);
    m_pVideoRenderer = new VideoRenderer(this);
    if (m_pVideoProvider)
        m_pVideoProvider->start();
    if (m_pVideoRenderer)
        m_pVideoRenderer->start();

//    producer.wait();
//    consumer.wait();
//    qDebug("end");
}

void VideoHolder::timerEvent(QTimerEvent * event)
{

}
