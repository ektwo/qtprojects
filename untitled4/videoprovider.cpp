#include "videoprovider.h"
#include "videoholder.h"

VideoProvider::VideoProvider(VideoHolder *pVideoHolder)
    : m_pVideoHolder(pVideoHolder)
    , m_frameIdx(0)
{
    DSG(1, "VideoProvider QThread::currentThreadId()=%d", QThread::currentThreadId());
}

void VideoProvider::run()
{
    //for (int i = 0; i < m_pVideoHolder->loopCount; i++)
    int available = 0;
    while (!isInterruptionRequested())
    {

        /* Tries to acquire n resources guarded by the semaphore.
         * If n > available(),
         * this call will block until enough resources are available.
         * 尝试去获取(减去)n个被信号量控制的资源。
         * 如果n>可用资源数量，它就会阻塞直到有足够的资源为止。 */
        //Bounded Buffer Producer/Consumer
        //當Buffer為滿，則Producer被迫wait
        m_pVideoHolder->m_semRequestSpace->acquire();
        //之所以lock要在acquire后面是因为: 如果消费者拿到了锁，
        //那么又没有商品，那么久会导致死锁
        m_pVideoHolder->mutex.lock();

        m_pVideoHolder->m_semProduct->release();

        available = m_pVideoHolder->m_semProduct->available();

        m_pVideoHolder->mutex.unlock();

        if (0 == m_frameIdx)
        {
            m_timerSlice.start();
        }
        DSG(1, "VideoProvider (%d) available=%d m_timerSlice=%d %d",
            m_frameIdx, available, m_timerSlice.elapsed(), (m_frameIdx * 33));

        ++m_frameIdx;
        msleep(5);
    }
    DSG(1, "VideoProvider::run() over");
}
