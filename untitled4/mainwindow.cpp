#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
DSG(1, "MainWindow QThread::currentThreadId()=%d", QThread::currentThreadId());
    SafeFreeThread *pSafe = new SafeFreeThread();
    m_Holder = new VideoHolder(nullptr);

    connect(this, &MainWindow::test, m_Holder, &VideoHolder::handleTest);
    if (m_Holder)
    {
        m_Holder->moveToThread(pSafe);//&m_safeThread);
        //m_safeThread.start();
        pSafe->start();
        //m_Holder->Test(100, 300);
        emit test(100, 15);
    }
}

MainWindow::~MainWindow()
{
    free (m_Holder);

    delete ui;
}

void MainWindow::on_actionStop_triggered()
{
    if (m_Holder)
        m_Holder->Stop();
}
