#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include "videoholder.h"


class SafeFreeThread final : public QThread
{
public:
    ~SafeFreeThread() { qDebug("SafeFreeThread"); quit(); wait(); qDebug("SafeFreeThread OK");
    }
};


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void test(int loopCount, int capacity);

private slots:
    void on_actionStop_triggered();

private:
    Ui::MainWindow *ui;
    VideoHolder *m_Holder;
    SafeFreeThread m_safeThread;
};

#endif // MAINWINDOW_H
