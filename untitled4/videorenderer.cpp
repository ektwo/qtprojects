#include "videorenderer.h"
#include "videoholder.h"
#include <Windows.h>

VideoRenderer::VideoRenderer(VideoHolder *pVideoHolder)
    : m_pVideoHolder(pVideoHolder)
    , m_queueFinished(false)
    , m_frameIdx(0)
{
    m_num = 1000;
    m_den = 30;
    DSG(1, "VideoRenderer QThread::currentThreadId()=%d", QThread::currentThreadId());
    //connect(this, SIGNAL(render()), this, SLOT(handleRender()));
}

void VideoRenderer::Render()
{
    DSG(1, "VideoRenderer Render QThread::currentThreadId()=%d", QThread::currentThreadId());
}

//void VideoRenderer::handleRender()
//{
//    DSG(1, "VideoRenderer handleRender QThread::currentThreadId()=%d", QThread::currentThreadId());
//}

void VideoRenderer::run()
{    DSG(1, "VideoRenderer run QThread::currentThreadId()=%d", QThread::currentThreadId());
    //for (int i = 0; i < m_pVideoHolder->loopCount; i++) {
    int available = 0;
    int time1 = MulDiv(1, m_num, m_den);
    int time1Div16 = time1 >> 4;
    int diff;
    int absDiff;
    int sleepTime;
    int predictedNextFrame;
    qint64 timeNow = 0;
    qint64 timeLast = 0;
    qint64 framedelta;
    while (!isInterruptionRequested())
    {
        //當Buffer為空，則Consumer被迫wait
        if (!m_queueFinished)
        {
            if (m_pVideoHolder->m_semProduct->available() < 15)
            {
                msleep(5);
                continue;
            }
            else
                m_queueFinished = true;
        }

        {
            m_pVideoHolder->m_semProduct->acquire();

            m_pVideoHolder->mutex.lock();

            m_pVideoHolder->m_semRequestSpace->release();

            available = m_pVideoHolder->m_semProduct->available();

            m_pVideoHolder->mutex.unlock();
        }
        {
            Render();
            //emit render();

            if (0 == m_frameIdx)
                m_timerSlice.start();

            predictedNextFrame = MulDiv(m_frameIdx + 1, m_num, m_den);
            timeNow = m_timerSlice.elapsed();
            //framedelta = (timeNow - timeLast);
            //timeLast = timeNow;
            framedelta = predictedNextFrame - timeNow; // framedelta > 0
            diff = (framedelta - time1);
            absDiff = abs(diff);

            //res = m_timerSlice.elapsed() - (m_frameIdx * 33);
            DSG(1, "VideoRenderer (%d) available=%d timeNow=%d framedelta=%d predictedNextFrame=%d",
                m_frameIdx, available, timeNow, framedelta, predictedNextFrame);

            if (0 == m_frameIdx)
                sleepTime = 0;
            else {
                //if (framedelta >= 0)
                {
                    if (absDiff < time1Div16)
                        sleepTime = time1;
                    else if (diff > 0) //ahead of prediction. slowly, sleep more.Expected to play later
                    {
                        //if (absDiff < (time1 / 5))
                            sleepTime = time1 + (diff / 5);
                        //else
                        //    sleepTime = time1 + (time1 / 5);
                    }
                    else // running a little behind. quickly, sleep less
                    {
                        //if (absDiff < (time1 / 5))
                            sleepTime = time1 + (diff / 5);
                        //else
                        //    sleepTime = time1 - (time1 / 5);
                    }
                }
            }
            DSG(1, "sleepTime=%d", sleepTime);
            msleep(sleepTime);

            ++m_frameIdx;
        }
    }

    DSG(1, "VideoRenderer::run() over");
}
