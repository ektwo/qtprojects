#ifndef VIDEOPROVIDER_H
#define VIDEOPROVIDER_H

#include <QThread>
#include <QElapsedTimer>

class VideoHolder;
class VideoProvider : public QThread
{
public:
    VideoProvider(VideoHolder *pVideoHolder);

protected:
    void run() override;

private:
    VideoHolder *m_pVideoHolder;
    int m_frameIdx;
    QElapsedTimer m_timerSlice;
};

#endif // VIDEOPROVIDER_H
