#ifndef VIDEORENDERER_H
#define VIDEORENDERER_H

#include <QThread>
#include <QElapsedTimer>

class VideoHolder;
class VideoRenderer : public QThread
{

public:
    VideoRenderer(VideoHolder *pVideoHolder);
    void Render();

protected:
    void run() override;

//signals:
//    void render();

//public slots:
//    void handleRender();

private:
    VideoHolder *m_pVideoHolder;
    bool m_queueFinished;
    int m_frameIdx;
    int m_num;
    int m_den;
    QElapsedTimer m_timerSlice;
};

#endif // VIDEORENDERER_H
