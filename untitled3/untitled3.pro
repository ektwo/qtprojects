#-------------------------------------------------
#
# Project created by QtCreator 2018-10-22T22:34:52
#
#-------------------------------------------------

QT       += core gui testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = untitled3
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11
#static { # everything below takes effect with CONFIG ''= static
    CONFIG += static
    # CONFIG += staticlib # this is needed if you create a static library, not a static executable
    DEFINES+= STATIC
     message("~~~ static build ~~~") # this is for information, that the static build is done

    win32: TARGET = $$join(TARGET,,,)
    QMAKE_LFLAGS_RELEASE = /NODEFAULTLIB:libcmtd.lib
    #this adds an s in the end, so you can seperate static build from non static build
#}
SOURCES += \
        main.cpp \
        mainwindow.cpp \
    shr_dataslave_thread.cpp \
    shr_retmaster_thread.cpp

HEADERS += \
        mainwindow.h \
    shr_dataslave_thread.h \
    shr_retmaster_thread.h

FORMS += \
        mainwindow.ui
USE_PYTHON3=1
    message(win32)
    message("QTDIR" = $(QTDIR))

    static { # everything below takes effect with CONFIG ''= static
        CONFIG += static
        # CONFIG += staticlib # this is needed if you create a static library, not a static executable
        DEFINES+= STATIC
         message("~~~ static build ~~~") # this is for information, that the static build is done

        win32: TARGET = $$join(TARGET,,,)
        QMAKE_LFLAGS_RELEASE = /NODEFAULTLIB:libcmtd.lib
        #this adds an s in the end, so you can seperate static build from non static build
    }
    BASELIB=$$PWD/lib


    # external library
    ZLIBPATH=D:/sourcecode/zlib-1.2.11-build
    ZLIBPATH_INC=$$ZLIBPATH/include
    ZLIBPATH_LIB=$$ZLIBPATH/lib
    message("ZLIBPATH_INC=" $$ZLIBPATH_INC)
    message("ZLIBPATH_LIB=" $$ZLIBPATH_LIB)

    PNGPATH=D:/sourcecode/libpng-1.6.34-build
    PNGPATH_INC=$$PNGPATH
    PNGPATH_LIB=$$PNGPATH/projects/vstudio2017/x64/ReleaseLibrary
    message("PNGPATH_INC=" $$PNGPATH_INC)
    message("PNGPATH_LIB=" $$PNGPATH_LIB)

    JPEGPATH=D:/sourcecode/jpegsr9c-build
    JPEGPATH_INC=$$JPEGPATH
    JPEGPATH_LIB=$$JPEGPATH
    message("JPEGPATH_INC=" $$JPEGPATH_INC)ilmimf.lib
    message("JPEGPATH_LIB=" $$JPEGPATH_LIB)

    INCLUDEPATH += $$ZLIBPATH_INC
    INCLUDEPATH += $$PNGPATH_INC
    INCLUDEPATH += $$JPEGPATH_INC
    DEPENDPATH += $$ZLIBPATH_INC
    DEPENDPATH += $$PNGPATH_INC
    DEPENDPATH += $$JPEGPATH_INC


    message("PYTHONDIR" = $(PYTHONDIR))
    # python section
    equals(USE_PYTHON3, 1) {
    PYTHON_PATH = $(PYTHONDIR)# D:/Application/Python/Python36
    }
    else {
    PYTHON_PATH = $(PYTHONDIR)# D:/Application/Python/Python36
    }
    message("PYTHON_PATH" = $$PYTHON_PATH)
    PYTHON_LIBS_PATH = $$PYTHON_PATH/libs

    INCLUDEPATH += $$PYTHON_PATH $$PYTHON_PATH/include
    INCLUDEPATH += $$PYTHON_PATH/Lib/site-packages/numpy/core/include/numpy
    DEPENDPATH += $$PYTHON_PATH $$PYTHON_PATH/include
    DEPENDPATH += $$PYTHON_PATH/Lib/site-packages/numpy/core/include/numpy



    # opencv section
    OPENCV_PATH = $(OPENCVDIR) # D:\sourcecode\opencv-2.4.13.6-build\install
    INCLUDEPATH += $$OPENCV_PATH/include
    DEPENDPATH += $$OPENCV_PATH/include
    OPENCV_LIBS_PATH = $$OPENCV_PATH/x64/vc15/staticlib

#    INCLUDEPATH += $$PWD/libyuv
#    LIBS += -L$$BASELIB -lyuv


    FFMPEGPATH=D:/sourcecode/ffmpeg/ffmpeg-4.0.2-win64-dev
    INCLUDEPATH += $$FFMPEGPATH/include
LIBS += avcodec.lib \
        avdevice.lib \
        avfilter.lib \
        avformat.lib \
        avutil.lib \
        postproc.lib \
        swresample.lib \
        swscale.lib


    # qt section
    INCLUDEPATH += $(QTDIR)/include

    equals(USE_SYS_QTSDK, "1") {
        LIBS += -Llib
    } else {
        LIBS += -L$(QTDIR)/lib
        LIBS += -lqtharfbuzz -lqtpcre2 \
    }
    message("INCLUDEPATH=" $$INCLUDEPATH)
    message("LIBS=" $$LIBS)
#    LIBS += \
#    -lQt5Widgets \
#    -lQt5Gui \
#    -lQt5Test \
#    -lQt5Concurrent \
#    -lQt5Core \
#    -lUxTheme \
#    -lwinmm \
#    -lVersion


    LIBS += -L$$OPENCV_LIBS_PATH
    LIBS += -lIlmImf \
    -llibjasper \
    -llibtiff \
    -lopencv_calib3d2413 \
    -lopencv_contrib2413 \
    -lopencv_core2413 \
    -lopencv_features2d2413 \
    -lopencv_flann2413 \
    -lopencv_gpu2413 \
    -lopencv_highgui2413 \
    -lopencv_imgproc2413 \
    -lopencv_legacy2413 \
    -lopencv_ml2413 \
    -lopencv_nonfree2413 \
    -lopencv_objdetect2413 \
    -lopencv_ocl2413 \
    -lopencv_photo2413 \
    -lopencv_stitching2413 \
    -lopencv_superres2413 \
    -lopencv_ts2413 \
    -lopencv_video2413 \
    -lopencv_videostab2413

    equals(USE_PYTHON3, 1) {
    LIBS += -L$$PYTHON_LIBS_PATH -lpython36
    }
    else {
    LIBS += -L$$PYTHON_LIBS_PATH -lpython27
    }

    LIBS += -L$$ZLIBPATH_LIB -lzlib
    LIBS += -L$$PNGPATH_LIB -llibpng
    LIBS += -L$$JPEGPATH_LIB -llibjpeg

    #-lopencv_world2413 \
    LIBS += vfw32.lib UxTheme.lib
    LIBS += -lopengl32 -lglu32



#    win32:!win32-g++: PRE_TARGETDEPS += $$BASELIB/yuv.lib
#    else:win32-g++: PRE_TARGETDEPS += $$BASELIB/yuv.lib
    win32:!win32-g++: PRE_TARGETDEPS += $$ZLIBPATH_LIB/zlib.lib
    else:win32-g++: PRE_TARGETDEPS += $$ZLIBPATH_LIB/libzlib.a
    win32:!win32-g++: PRE_TARGETDEPS += $$PNGPATH_LIB/libpng.lib
    else:win32-g++: PRE_TARGETDEPS += $$PNGPATH_LIB/liblibpng.a
    win32:!win32-g++: PRE_TARGETDEPS += $$JPEGPATH_LIB/libjpeg.lib
    else:win32-g++: PRE_TARGETDEPS += $$JPEGPATH_LIB/liblibjpeg.a
    win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/IlmImf.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libIlmImf.a
    win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libjasper.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/liblibjasper.a
    win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libtiff.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/liblibtiff.a
    win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_calib3d2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_calib3d2413.a
    win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_contrib2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_contrib2413.a
    win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_core2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_core2413.a
    win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_features2d2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_features2d2413.a
    win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_flann2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_flann2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_gpu2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_gpu2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_highgui2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_highgui2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_imgproc2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_imgproc2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_legacy2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_legacy2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_ml2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_ml2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_nonfree2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_nonfree2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_objdetect2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_objdetect2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_ocl2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_ocl2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_photo2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_photo2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_stitching2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_stitching2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_superres2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_superres2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_ts2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_ts2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_video2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_video2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_videostab2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_videostab2413.a
    equals(USE_PYTHON3, 1) {
    win32:!win32-g++: PRE_TARGETDEPS += $$PYTHON_LIBS_PATH/python36.lib
    else:win32-g++: PRE_TARGETDEPS += $$PYTHON_LIBS_PATH/libpython36.a
    }
    else {
    win32:!win32-g++: PRE_TARGETDEPS += $$PYTHON_LIBS_PATH/python27.lib
    else:win32-g++: PRE_TARGETDEPS += $$PYTHON_LIBS_PATH/libpython27.a
    }

#    INCLUDEPATH += D:/sourcecode/czmq/czmq/include
#    DEPENDPATH += D:/sourcecode/czmq/czmq/include
    INCLUDEPATH += $$PWD/../../../../sourcecode/czmq/czmq/include
    DEPENDPATH += $$PWD/../../../../sourcecode/czmq/czmq/include
    INCLUDEPATH += $$PWD/../../../../sourcecode/czmq/libzmq/include
    DEPENDPATH += $$PWD/../../../../sourcecode/czmq/libzmq/include

    win32: LIBS += -L$$PWD/../../../../sourcecode/czmq/libzmq/bin/x64/Release/v141/static/ -llibzmq
    win32: LIBS += -L$$PWD/../../../../sourcecode/czmq/czmq/bin/x64/Release/v141/static/ -llibczmq

    win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../../../sourcecode/czmq/libzmq/bin/x64/Release/v141/static/libzmq.lib
    else:win32-g++: PRE_TARGETDEPS += $$PWD/../../../../sourcecode/czmq/libzmq/bin/x64/Release/v141/static/liblibzmq.a
    win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../../../sourcecode/czmq/czmq/bin/x64/Release/v141/static/libczmq.lib
    else:win32-g++: PRE_TARGETDEPS += $$PWD/../../../../sourcecode/czmq/czmq/bin/x64/Release/v141/static/liblibczmq.a
