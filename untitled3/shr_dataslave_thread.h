#ifndef SHR_DATASLAVE_THREAD_H
#define SHR_DATASLAVE_THREAD_H

#pragma once

#include <qthread.h>
#include <qsystemsemaphore.h>
#include <qsharedmemory.h>
#include <QVector>
#include <QByteArray>
#include <QRect>
#include <QBuffer>

class ShrDataSlaveThread : public QThread
{
    Q_OBJECT
public:
    explicit ShrDataSlaveThread(QObject * parent = nullptr);
    ~ShrDataSlaveThread();
    bool Initialize(const char *strMem, const char *strSemSrc, const char *strSemDst, bool bCreated);

signals:
    void sgDeliverBox(quint32, bool , const QVector<QRect> &);
    void sgiQuit();

protected:
    bool read();
    void run() override;

public:
    bool m_running;
    QSharedMemory	 *m_mem;
    QSystemSemaphore *m_semSrc;
    QSystemSemaphore *m_semDst;
    QVector<QBuffer> m_cacheImgList;
};

#endif // THRD_H
