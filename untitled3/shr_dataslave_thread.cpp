#include "shr_dataslave_thread.h"
#include <qbuffer.h>
#include <qdatastream.h>
#include <QDebug>
#include <QRect>
#include "opencv2/opencv.hpp"

#define SC(type, expr) static_cast<type>(expr)
#define CC(type, expr) const_cast<type>(expr)
#define RC(type, expr) reinterpret_cast<type>(expr)
#define DC(type, expr) dynamic_cast<type>(expr)

#ifdef QT_VERSION
#include <QDebug>
//#define DSG(mode, __format__, ...) { if (1 == mode) qDebug(__format__, ##__VA_ARGS__); }
#define DSG(mode, __format__, ...) { if (1 == mode) printf(__format__ "\r\n", ##__VA_ARGS__); }
#else
#ifdef __linux__
#define DSG(mode, __format__, ...) { if (1 == mode) printf(__format__, ##__VA_ARGS__); }
#elif _WIN32
#include <Windows.h>
#define MAX_DBG_MSG_LEN (1024)
static void DSG(int mode, const char * format, ...)
{
    if (1 == mode) {
        char buf[MAX_DBG_MSG_LEN];
        va_list ap;
        va_start(ap, format);
        _vsnprintf(buf, sizeof(buf), format, ap);
        va_end(ap);
        OutputDebugStringA(buf);
    }
}
#include <tchar.h>
static void DSGW(int mode, const TCHAR *format, ...)
{
    if (1 == mode) {
        TCHAR buf[MAX_DBG_MSG_LEN];
        va_list ap;
        va_start(ap, format);
        _vsntprintf_s(buf, sizeof(buf), format, ap);
        va_end(ap);
        OutputDebugString(buf);
    }
}
#endif
#endif


ShrDataSlaveThread::ShrDataSlaveThread(QObject *parent)
    : QThread(parent)
    , m_running(true)
{
}

ShrDataSlaveThread::~ShrDataSlaveThread()
{
    if (m_semSrc)
    {
        delete m_semSrc;
        m_semSrc = nullptr;
    }
    if (m_semDst)
    {
        delete m_semDst;
        m_semDst = nullptr;
    }
    if (m_mem)
    {
        delete m_mem;
        m_mem = nullptr;
    }
    qDebug("ShrDataSlaveThread::~ShrDataSlaveThread");
}


bool ShrDataSlaveThread::Initialize(const char *strMem, const char *strSemSrc, const char *strSemDst, bool bCreated)
{
    bool ret = false;
    m_mem = new QSharedMemory(QString(strMem));
    if (m_mem)
    {
        ret = m_mem->attach();
        if (!ret) {
            DSG(1, "ShrRetMasterThread::Initialize m_mem->attach error=%d ret=%s",
                m_mem->error(), m_mem->errorString().toLatin1().constData());
        } else
        {
            DSG(1, "ShrDataSlaveThread::Initialize attach OK");
            if (bCreated) {
                m_semSrc = new QSystemSemaphore(QString(strSemSrc), 1, QSystemSemaphore::Create);
                m_semDst = new QSystemSemaphore(QString(strSemDst), 0, QSystemSemaphore::Create);
            } else {
                m_semSrc = new QSystemSemaphore(QString(strSemSrc), 1, QSystemSemaphore::Open);
                m_semDst = new QSystemSemaphore(QString(strSemDst), 0, QSystemSemaphore::Open);
            }
            ret = true;
        }
    }

    return ret;
}

bool ShrDataSlaveThread::read()
{
    //qDebug("ShrDataSlaveThread::read");
    bool continued = true;
    DSG(1, "ShrDataSlaveThread::read 1 m_mem->size()=%d", m_mem->size());
//    if(m_mem->isAttached())
//    {
//        printf("ShrDataSlaveThread::read haved attached\r\n");
//        //qDebug() << "ReadSharedMemory:: haved attached.";
//    }
//    else
//    {
//        if(!m_mem->attach())
//        {
//            QSharedMemory::SharedMemoryError m = m_mem->error();
//            printf("ShrDataSlaveThread::read haved error=%d\r\n", m);
//            return false;
//        }
//        else
//        {
//            printf("ShrDataSlaveThread::read attach success\r\n");
//            qDebug() << "ReadSharedMemory:: attach success.";
//        }
//    }

    //static int dstFrameIdx = 0;
    DSG(1, "ShrDataSlaveThread::read m_semDst->acquire()");
    if(m_semDst->acquire())
    {
        DSG(1, "ShrDataSlaveThread::read m_semDst->acquire() pass");
        QBuffer buffer;
        QDataStream in(&buffer);

        m_mem->lock();

        //cv::Mat frameAAA = cv::Mat(720, 1280, CV_8UC3, (uint8_t*)m_mem->constData());
        //imshow("frameAAA", frameAAA);
        DSG(1, "ShrDataSlaveThread::read m_mem->size()=%d", m_mem->size());
        const uint8_t *pData = SC(const uint8_t*, m_mem->constData());
        quint32 frameIdx = *(reinterpret_cast<const quint32*>(pData));
        quint32 indicator = *(reinterpret_cast<const quint32*>(&pData[4]));
        DSG(1, "frameIdx=%d", frameIdx);
        DSG(1, "indicator=%d", indicator);
        //qDebug("ShrDataSlaveThread::read m_semDst->acquire() frameIdx=%u indicator=%u",frameIdx,indicator);
//        if ((0xFFFFFFFF == frameIdx) && (indicator == frameIdx))
//        {
//            //qDebug("ShrDataSlaveThread::read quit signal 1");
//            QVector<QRect> rects;
//            emit sgDeliverBox(frameIdx, 1, rects);

//            m_mem->unlock();

//            m_semSrc->release();

//            m_running = false;
//            //emit sgiQuit();
//            qDebug("ShrDataSlaveThread::read quit signal 2");
//            return ;
//        }
        QVector<QRect> rects;
        if ((0xFFFFFFFF == frameIdx) && (indicator == frameIdx))
        {
            emit sgDeliverBox(frameIdx, 1, rects);
            continued = false;
        }
        else
        {
            QRect rect1(20, 20, 100, 80);
            QRect rect2 = QRect(120, 120, 300, 380);
            QRect rect3 = QRect(260, 260, 100, 80);
            QRect rect4 = QRect(580, 580, 100, 80);

            rects.push_back(rect1);
            rects.push_back(rect2);
            rects.push_back(rect3);
            rects.push_back(rect4);

            emit sgDeliverBox(frameIdx, 0, rects);
        }

        m_mem->unlock();

        m_semSrc->release();

        DSG(1, "ShrDataSlaveThread::read m_semSrc->release");
    }
    return continued;
}

void ShrDataSlaveThread::run()
{
    do
    {
        msleep(1);
        if (!read())
            break;
        //qDebug("ShrDataSlaveThread::run m_running=%d",m_running);
    } while(m_running);
    qDebug("ShrDataSlaveThread::run over");
}

//void ShrDataSlaveThread::write(uint8_t *pBuffer, size_t length)
//{
//    fwrite(pBuffer, 1, length, stdout);
//    fflush(stdout);
//}
