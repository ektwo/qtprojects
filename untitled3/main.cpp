#include "mainwindow.h"
#include <QApplication>
#include <qDebug>
#include <fcntl.h>
#include <io.h>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::addLibraryPath("./plugins");

    setvbuf(stdout, static_cast<char *>(nullptr), _IONBF, 0);
    setvbuf(stderr, static_cast<char *>(nullptr), _IONBF, 0);
    //_setmode(_fileno(stdout), _O_BINARY);
    //_setmode(_fileno(stderr), _O_BINARY);

    //QSystemSemaphore sem("market", 5, QSystemSemaphore::Create);


    MainWindow w;
    w.show();

    return a.exec();
}
