#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QVector>

#include <QRect>

static const char *k_str_shared_memory_data = "sharedmemory_data00";
static const char *k_str_sem_src_data = "sem_src_data";
static const char *k_str_sem_dst_data = "sem_dst_data";

static const char *k_str_shared_memory_ret = "sharedmemory_ret0";
static const char *k_str_sem_src_ret = "sem_src_ret";
static const char *k_str_sem_dst_ret = "sem_dst_ret";

Q_DECLARE_METATYPE(QVector<QRect>)

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qRegisterMetaType<QVector<QRect>>();

//    QMessageBox msg;
//    msg.setText("start");
//    msg.exec();

    m_pEdt = new QLineEdit(this);

    if (m_dataSlaveTrd.Initialize(k_str_shared_memory_data, k_str_sem_src_data, k_str_sem_dst_data, false))
        m_dataSlaveTrd.start();
    if (m_retMasterTrd.Initialize(k_str_shared_memory_ret, k_str_sem_src_ret, k_str_sem_dst_ret, false))
        m_retMasterTrd.start();

//    connect(&m_dataSlaveTrd, &ShrDataSlaveThread::sgDeliverBox,
//            &m_retMasterTrd, &ShrRetMasterThread::soDeliverBox);

//    connect(&m_dataSlaveTrd, SIGNAL(sgiQuit()),
//            &m_retMasterTrd, SLOT(soiQuit()));

    connect(&m_retMasterTrd, &ShrRetMasterThread::sgiQuitToo,
            this, &MainWindow::close);

//    connect(&m_retMasterTrd, &ShrRetMasterThread::sgiQuitToo,
//            this, &MainWindow::soAllQuit);
//    connect(&m_retMasterTrd, &ShrRetMasterThread::sgiQuitToo,
//            this, [] () {});
}

MainWindow::~MainWindow()
{
    m_dataSlaveTrd.terminate();
    delete ui;
}

void MainWindow::OnRecv(QByteArray qba)
{
    //qDebug("OnRecv %s", QString(qba).toLatin1().constData());
    m_pEdt->setText(QString(qba));
}

void MainWindow::soAllQuit()
{

}

void MainWindow::resizeEvent(QResizeEvent *e)
{
    m_pEdt->setGeometry(width() / 10, height()/10, width() * 0.8, 20);
}
