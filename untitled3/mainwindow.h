#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>
#include <QResizeEvent>
#include "shr_retmaster_thread.h"
#include "shr_dataslave_thread.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    ShrRetMasterThread m_retMasterTrd;
    ShrDataSlaveThread m_dataSlaveTrd;
    QLineEdit   *m_pEdt;

public slots:
    void OnRecv(QByteArray);
    void soAllQuit();

protected:
    void resizeEvent(QResizeEvent *);
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
