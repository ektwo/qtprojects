#include "shr_retmaster_thread.h"
#include <qbuffer.h>
#include <qdatastream.h>
#include <QDebug>
#include <QVector>
#include <QRect>
#include "opencv2/opencv.hpp"


#define SC(type, expr) static_cast<type>(expr)
#define CC(type, expr) const_cast<type>(expr)
#define RC(type, expr) reinterpret_cast<type>(expr)
#define DC(type, expr) dynamic_cast<type>(expr)
#define bswap_16(value) \
((((value) & 0xff) << 8) | ((value) >> 8))


#ifdef QT_VERSION
#include <QDebug>
//#define DSG(mode, __format__, ...) { if (1 == mode) qDebug(__format__, ##__VA_ARGS__); }
#define DSG(mode, __format__, ...) { if (1 == mode) printf(__format__ "\r\n", ##__VA_ARGS__); }
#else
#ifdef __linux__
#define DSG(mode, __format__, ...) { if (1 == mode) printf(__format__, ##__VA_ARGS__); }
#elif _WIN32
#include <Windows.h>
#define MAX_DBG_MSG_LEN (1024)
static void DSG(int mode, const char * format, ...)
{
    if (1 == mode) {
        char buf[MAX_DBG_MSG_LEN];
        va_list ap;
        va_start(ap, format);
        _vsnprintf(buf, sizeof(buf), format, ap);
        va_end(ap);
        OutputDebugStringA(buf);
    }
}
#include <tchar.h>
static void DSGW(int mode, const TCHAR *format, ...)
{
    if (1 == mode) {
        TCHAR buf[MAX_DBG_MSG_LEN];
        va_list ap;
        va_start(ap, format);
        _vsntprintf_s(buf, sizeof(buf), format, ap);
        va_end(ap);
        OutputDebugString(buf);
    }
}
#endif
#endif


ShrRetMasterThread::ShrRetMasterThread(QObject * parent)
    : QThread(parent)
    , m_running(true)
    , m_askAbort(false)
    , m_bufferSize(4 + 4 + sizeof(uint32_t) * 4 * 30)
    , m_mem(nullptr)
    , m_semSrc(nullptr)
    , m_semDst(nullptr)
{

}

ShrRetMasterThread::~ShrRetMasterThread()
{
    if (m_semSrc)
    {
        delete m_semSrc;
        m_semSrc = nullptr;
    }
    if (m_semDst)
    {
        delete m_semDst;
        m_semDst = nullptr;
    }
    if (m_mem)
    {
        delete m_mem;
        m_mem = nullptr;
    }
    qDebug("ShrRetMasterThread::~ShrRetMasterThread");
}

bool ShrRetMasterThread::Initialize(const char *strMem, const char *strSemSrc, const char *strSemDst, bool bCreated)
{
    bool ret = false;

    m_mem = new QSharedMemory(QString(strMem));
    if (m_mem)
    {
        ret = m_mem->attach();
        if (!ret) {
            DSG(1, "ShrRetMasterThread::Initialize m_mem->attach error=%d ret=%s",
                m_mem->error(), m_mem->errorString().toLatin1().constData());
        } else
        {
            DSG(1, "ShrRetMasterThread::Initialize attach OK");
            if (bCreated) {
                m_semSrc = new QSystemSemaphore(QString(strSemSrc), 1, QSystemSemaphore::Create);
                m_semDst = new QSystemSemaphore(QString(strSemDst), 0, QSystemSemaphore::Create);
            } else {
                m_semSrc = new QSystemSemaphore(QString(strSemSrc), 1, QSystemSemaphore::Open);
                m_semDst = new QSystemSemaphore(QString(strSemDst), 0, QSystemSemaphore::Open);
            }
            ret = true;
        }
    }
//    m_mem = new QSharedMemory(QString(strMem));
//    if (m_mem)
//    {
//        DSG(1, "ShrRetMasterThread::Initialize memsize=%d", m_bufferSize);
//        ret = m_mem->create(m_bufferSize);
//        DSG(1, "ShrRetMasterThread::Initialize m_mem->create ret=%d", ret);
//        if (!ret) {
//            while (m_mem->error() == QSharedMemory::AlreadyExists) {
//                m_mem->setKey(m_mem->key() + QLatin1String("0"));

//                ret = m_mem->create(m_bufferSize);
//                if (!ret)
//                {
//                    DSG(1, "ShrRetMasterThread::Initialize m_mem->create error ret=%s", m_mem->errorString().toLatin1().constData());
//                }
//                else
//                    DSG(1, "ShrRetMasterThread::Initialize new name=%s", m_mem->key().toLatin1().constData());
//            }

//        }
//        if (ret)
//        {
//            DSG(1, "ShrRetMasterThread::Initialize size=%d", m_mem->size());
//            ret = m_mem->attach();
//            if (!ret) {
//                DSG(1, "ShrRetMasterThread::Initialize m_mem->attach error=%d ret=%s",
//                    m_mem->error(), m_mem->errorString().toLatin1().constData());
//            }
//            ret = true;
//        }
//    }

//    if (ret)
//    {
//        DSG(1, "ShrRetMasterThread::Initialize init QSystemSemaphore");
//        if (bCreated) {
//            m_semSrc = new QSystemSemaphore(QString(strSemSrc), 1, QSystemSemaphore::Create);
//            m_semDst = new QSystemSemaphore(QString(strSemDst), 0, QSystemSemaphore::Create);
//        } else {
//            m_semSrc = new QSystemSemaphore(QString(strSemSrc), 1, QSystemSemaphore::Open);
//            m_semDst = new QSystemSemaphore(QString(strSemDst), 0, QSystemSemaphore::Open);
//        }
//    }

    return ret;
}

void ShrRetMasterThread::soiQuit()
{
//    //m_semDst->release();
//    qDebug("ShrDataMasterThread::Release 1");
//    m_askAbort = true;
//    QVector<QRect> zero;
//    soDeliverBox(-1, zero);
//    qDebug("ShrDataMasterThread::Release 2 m_askAbort.load()=%d",m_askAbort.load());
//    //m_semSrc->release();
//    qDebug("ShrDataMasterThread::Release 3");
//    msleep(100);
//    m_running = false;
//    qDebug("ShrDataMasterThread::Release 4");

//    qDebug("ShrRetMasterThread::soiQuit 1");
//    m_running = false;
//    qDebug("ShrRetMasterThread::soiQuit 2");
//    emit sgiQuitToo();
//    qDebug("ShrRetMasterThread::soiQuit 3");
}
//void ShrRetMasterThread::soDeliverBox(quint32 frameIdx, bool quitSignal, const QVector<QRect> &rects)
void ShrRetMasterThread::soDeliverBox(quint32 frameIdx, bool quitSignal, const QVector<QRect> &rects)
{
    //qDebug("ShrRetMasterThread::soDeliverBox");
    if (m_semSrc->acquire())
    {
        //qDebug("m_retMasterTrd::soDeliverBox m_semSrc->acquire() ok rects.size()=%d", rects.size());
        m_mem->lock();

        if ((0xFFFFFFFF == frameIdx) && (quitSignal))
        {
            //qDebug("ShrRetMasterThread::soDeliverBox quit signal 1");
            m_mem->unlock();
            m_semDst->release();
            m_running = false;
            //qDebug("ShrRetMasterThread::soDeliverBox quit signal 2");
            return ;
        }

        if (rects.size() < 30)
        {
            quint32 *pData32 = SC(quint32*, m_mem->data());
            //uint8_t *pData = SC(uint8_t*, m_mem->data());
            pData32[0] = SC(quint32, frameIdx);
            //memcpy(pData, SC(void* const, &frameIdx), 4);
            pData32[1] = SC(quint32, rects.size());
//            qDebug("XXX 1 2 pData[0]=%x %x %x %x %x %x %x %x",
//                   pData[0],pData[1],pData[2],pData[3],
//                    pData[4],pData[5],pData[6],pData[7]);
            //pData += 4;
            //pData[0] = static_cast<uint8_t>(rects.size());
            pData32 += 2;
            //pData += 8;
            //uint16_t *pData16 = RC(uint16_t*, pData);
            for (int i = 0; i < rects.size(); ++i)
            {
                const QRect *pRect = &rects.at(i);
                pData32[0] = SC(quint32, pRect->left());
                pData32[1] = SC(quint32, pRect->top());
                pData32[2] = SC(quint32, pRect->left() + pRect->width());
                pData32[3] = SC(quint32, pRect->top() + pRect->height());
//                qDebug("XXX 3 pData[0]=%d %d %d %d %d %d %d %d",
//                       pData[0],pData[1],pData[2],pData[3],
//                        pData[4],pData[5],pData[6],pData[7]);
                //pData += 16;
//                pData[0] = static_cast<uint8_t>(pRect->left() >> 8);
//                pData[1] = static_cast<uint8_t>(pRect->left() & 0xFF);
//                pData[2] = static_cast<uint8_t>(pRect->top() >> 8);
//                pData[3] = static_cast<uint8_t>(pRect->top() & 0xFF);
//                pData[4] = static_cast<uint8_t>((pRect->left() + pRect->width()) >> 8);
//                pData[5] = static_cast<uint8_t>((pRect->left() + pRect->width()) & 0xFF);
//                pData[6] = static_cast<uint8_t>((pRect->top() + pRect->height()) >> 8);
//                pData[7] = static_cast<uint8_t>((pRect->top() + pRect->height()) & 0xFF);
                pData32 += 4;
            }
        }

        m_mem->unlock();

        m_semDst->release();
        //qDebug("m_retMasterTrd::soDeliverBox m_semDst->release()");
    }
}

void ShrRetMasterThread::run()
{
    while(m_running)
        msleep(1);
    qDebug("ShrRetMasterThread::run over");
}
