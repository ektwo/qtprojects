#ifndef SHR_RETMASTER_THREAD_H
#define SHR_RETMASTER_THREAD_H

#pragma once

#include <qthread.h>
#include <qsystemsemaphore.h>
#include <qsharedmemory.h>
#include <QRect>

class ShrRetMasterThread : public QThread
{
    Q_OBJECT
public:
    explicit ShrRetMasterThread(QObject * parent = nullptr);
    ~ShrRetMasterThread();
    bool Initialize(const char *strMem, const char *strSemSrc, const char *strSemDst, bool bCreated);

signals:
    void sgiQuitToo();

public slots:
    //void soDeliverBox(quint32, bool, QVector<QRect>);
    void soDeliverBox(quint32, bool, const QVector<QRect> &);
    void soiQuit();

protected:
    void run();

public:
    bool m_running;
    bool m_askAbort;
    int  m_bufferSize;
    QSharedMemory	 *m_mem;
    QSystemSemaphore *m_semSrc;
    QSystemSemaphore *m_semDst;
};

#endif
